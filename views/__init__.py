from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_sqlalchemy import SQLAlchemy

from database_models.contact import Contact
from database_models.person import Person
from views.contact import ContactView
from views.person import PersonView


def add_views(admin_app: Admin, db: SQLAlchemy):
    admin_app.add_view(PersonView(Person, db.session))
    admin_app.add_view(ContactView(Contact, db.session))
