import logging
import os
from dotenv import find_dotenv, load_dotenv

from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from configuration import EnvironmentMapper
from configuration.database import db
from configuration.extensions import migrate
from database_models.person import Person

logger = logging.getLogger(__name__)
PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))
release_level = os.getenv('FAD_RELEASE_LEVEL', 'local').lower()


ENV_FILE = find_dotenv()

if os.path.isfile(ENV_FILE + ".%s" % release_level):
    ENV_FILE = ENV_FILE + ".%s" % release_level

if ENV_FILE:
    load_dotenv(ENV_FILE)

app = Flask(__name__, instance_relative_config=True)

# Pro-tip: centralize your configurations
if release_level and release_level in EnvironmentMapper:
    app.config.from_object(EnvironmentMapper[release_level])

# !!!! OVERRIDE !!!
# N.B. Checkout: https://bootswatch.com/3/
app.config['FLASK_ADMIN_SWATCH'] = 'darkly'

# DB Setup
db.init_app(app=app)
migrate.init_app(app=app, db=db)


# Setup Flask-Admin
# N.B. uncomment out for default /admin endpoint
# admin = Admin(app=app, name="BasicAdmin", template_mode="bootstrap3")
admin = Admin(app=app, name="BasicAdmin", template_mode="bootstrap3", url="/")

# Our first view!
admin.add_view(ModelView(Person, db.session))

if __name__ == '__main__':
    app.run()
